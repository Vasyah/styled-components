import React, { FC, useState } from "react";
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";
import { Title } from "./components/Title";
import { Flex } from "./components/Flex";
import { Console } from "./components/Console";
import { Button } from "./components/Button";
import { themeStore } from "./theme/theme";
import { observer } from "mobx-react";
import { IPalette } from "./theme/types/default";
import { store } from "./store/store";

const Global = createGlobalStyle`
  * {
    font-family: "Bricks";
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font-size: 16px;
  }
`;

export interface IAppWrapper {
  backgroundColor?: string;
}

const AppWrapper = styled.div<IAppWrapper>`
  min-height: 100vh;
  padding: 2rem;
  background-color: ${({ backgroundColor, theme }) =>
    backgroundColor || theme.palette.primary.contrastText};
  color: #fff;
`;

const App: FC = observer(() => {
  const [theme, setTheme] = useState(themeStore);
  const whiteTheme: IPalette = { main: "#000", contrastText: "#B064CA" };
  const resetTheme: IPalette = { ...store.primary };
  const updateTheme = (colors: IPalette) => {
    setTheme({ ...theme, palette: { ...theme.palette, primary: colors } });
  };

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Global />
        <AppWrapper backgroundColor={"#E9EAED"}>
          <Flex justify={"center"}>
            <Title>That's a title</Title>
          </Flex>
          <Console />
          <Flex justify={"center"} gap={"2rem"}>
            <Button onClick={() => updateTheme(whiteTheme)} primary>
              Change Theme To white
            </Button>
            <Button onClick={() => updateTheme(resetTheme)} outlined>
              Reset To Default
            </Button>
          </Flex>
        </AppWrapper>
      </div>
    </ThemeProvider>
  );
});

export default App;
