import React, { FC } from "react";
import styled, { css, keyframes } from "styled-components";

const translate = keyframes`
  0% {
    transform: translateY(0);
  }
  100% {
    transform: translateY(20px);
  }`;

const StyledButton = styled.button<IButtonProps>`
  min-width: 180px;
  max-width: 300px;
  padding: 1.5rem 0.5rem;
  margin-top: 2rem;
  font-size: 1.3rem;
  cursor: pointer;
  outline: none;
  transition: all 0.3s;

  ${(props) =>
    props.outlined &&
    css<IButtonProps>`
      color: ${({ color, theme }) => color || theme.palette.primary.main};
      border: ${({ color, theme }) =>
        `2px solid ${color || theme.palette.primary.contrastText}`};
      background-color: transparent;

      &:hover {
        color: ${({ color, theme }) => color || theme.palette.primary.main};
        border: 2px solid transparent;
        background-color: ${({ backgroundColor, theme }) =>
          backgroundColor || theme.palette.primary.contrastText};
        animation: ${translate} 1s infinite;
      }
    `}
  ${(props) =>
    props.primary &&
    css<IButtonProps>`
      color: ${({ color, theme }) => color || theme.palette.primary.main};
      border: 2px solid transparent;
      background-color: ${({ backgroundColor, theme }) =>
        backgroundColor || theme.palette.primary.contrastText};

      &:hover {
        color: ${({ color, theme }) => color || theme.palette.primary.main};
        border: ${({ color, theme }) =>
          `2px solid ${color || theme.palette.primary.main}`};
        background-color: transparent;
        animation: ${translate} 1s infinite;
      }
    `}
  &:focus {
    border: none;
    outline: none;
  }
`;
//
// const LargeButton = styled(StyledButton)`
//   font-size: 2rem;
// `;
//
// const RoundedButton = styled(StyledButton)`
//   border-radius: 1rem;
// `;

export interface IButtonProps
  extends Omit<
    React.DetailedHTMLProps<
      React.ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >,
    "ref"
  > {
  primary?: boolean;
  outlined?: boolean;
  color?: string;
  backgroundColor?: string;
}

export const Button: FC<IButtonProps> = (props) => {
  return <StyledButton {...props}>{props.children}</StyledButton>;
};
