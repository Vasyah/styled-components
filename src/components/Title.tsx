import React, { FC } from "react";
import styled from "styled-components";

const StyledTitle = styled.h1<ITitleProps>`
  color: ${({ color, theme }) => color || theme.palette.primary.main};
`;

export interface ITitleProps {
  className?: string;
  color?: string;
}

export const Title: FC<ITitleProps> = (props) => {
  return <StyledTitle {...props}>{props.children}</StyledTitle>;
};
