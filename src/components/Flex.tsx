import React, { FC } from "react";
import styled, { CSSProperties } from "styled-components";

const StyledFlex = styled.div<IFlexProps>`
  display: flex;
  flex-direction: ${({ direction }) => direction || "row"};
  align-items: ${({ align }) => align || "flex-start"};
  justify-content: ${({ justify }) => justify || "flex-start"};
  gap: ${({ gap }) => gap || "0"};
  flex-wrap: ${({ wrap }) => wrap || "initial"};
  flex: ${({ flex }) => flex || "initial"};
`;

export interface IFlexProps {
  className?: string;
  align?: CSSProperties["alignItems"];
  justify?: CSSProperties["justifyContent"];
  direction?: CSSProperties["flexDirection"];
  gap?: CSSProperties["gap"];
  wrap?: CSSProperties["flexWrap"];
  flex?: CSSProperties["flex"];
}

export const Flex: FC<IFlexProps> = (props) => {
  return <StyledFlex {...props}>{props.children}</StyledFlex>;
};
