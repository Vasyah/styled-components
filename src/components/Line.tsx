import React, { FC } from "react";
import styled from "styled-components";

const StyledLine = styled.div<ILineProps>`
  font-size: 1rem;
  color: ${({ color,theme }) => color  || theme.palette.primary.main};
`;

export interface ILineProps {
  color?: string;
}

export const Line: FC<ILineProps> = (props) => {
  return <StyledLine {...props}>{props.children}</StyledLine>;
};
