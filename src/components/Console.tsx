import React, { FC, useState } from "react";
import styled from "styled-components";
import { Flex } from "./Flex";
import { Line } from "./Line";

const StyledConsole = styled.textarea<IConsoleProps>`
  width: 100%;
  max-width: 100%;
  font-size: 1rem;
  height: 50vh;
  background-color: ${({ backgroundColor, theme }) =>
    backgroundColor || theme.palette.primary.contrastText};
  color: ${({ color, theme }) => color || theme.palette.primary.main};
  border: none;

  &:focus {
    border: none;
    outline: none;
  }
`;

export interface IConsoleProps {
  color?: string;
  backgroundColor?: string;
}

export const Console: FC<IConsoleProps> = ({ color, ...props }) => {
  const [lines, setLines] = useState(["C/users/goidini"]);

  const onEnterPress = (e: React.KeyboardEvent) =>
    e.charCode === 13 && setLines([...lines, "C/users/goidini"]);

  return (
    <Flex gap={"1rem"}>
      <Flex direction={"column"}>
        {lines.map((line) => (
          <Line>{line}</Line>
        ))}
      </Flex>

      <StyledConsole onKeyPress={onEnterPress} color={color} {...props}>
        {props.children}
      </StyledConsole>
    </Flex>
  );
};
