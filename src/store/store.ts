import { observable, makeAutoObservable } from "mobx";
import { IPalette } from "../theme/types/default";

export interface IRootStore {
  primary: IPalette;
  secondary: IPalette;
  theme?: string;
}

class UserStore implements IRootStore {
  @observable primary = {
    main: "#F8B9A3",
    contrastText: "#3E4245",
  };

  @observable secondary = {
    main: "#709fb0",
    contrastText: "#ffffff",
  };

  constructor() {
    makeAutoObservable(this);
  }

  changePrimaryTheme: (colors: IPalette) => void = (primary: IPalette) => {
    this.primary = {
      ...this.primary,
      ...primary,
    };
  };

  changeSecondaryTheme: (colors: IPalette) => void = (primary: IPalette) => {
    this.secondary = {
      ...this.secondary,
      ...primary,
    };
  };
}

export const store = new UserStore();
