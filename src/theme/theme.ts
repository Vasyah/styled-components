import { DefaultTheme } from "styled-components";
import { store } from "../store/store";

export const themeStore: DefaultTheme = {
  borderRadius: "4px",
  palette: {
    common: {
      black: "#222831",
      white: "#ffffff",
    },
    primary: {
      ...store.primary,
    },
    secondary: {
      ...store.secondary,
    },
  },
};
